Fifa14Client
===========
Fifa14Client is a python library that allows you to interact with the Fifa 14 Ultimate Web App programmatically.

Features
=========
* Logging in
* Getting Coin Amount
* Searching
* Buying
* Listing for auction
* Quick Selling
* Getting Tradepile,Unassigned pile, and Watchlist
* Moving bewteen piles
* Removing from watchlist
* Removing from tradepile

Prerequisites
========
* [requests 2.x](http://www.python-requests.org/en/latest/)
* [Python 2.x](http://www.python.org/download/releases/2.7.6)

Example Usage
===========
You can find an example of usage in the main.py, but anyways:

```python
from Fifa14Client import LoginManager
from Fifa14Client import WebAppFunctioner
import ConfigParser
from extra import EAHash


def do_main():
    Config = ConfigParser.ConfigParser()
    Config.read("accounts_example.ini")
    for section in Config.sections():
        email = Config.get(section, 'Email')
        password = Config.get(section, 'Password')
        secret_answer = Config.get(section, 'Secret')
        security_hash = EAHash.EAHashingAlgorithm().EAHash(secret_answer)
        platform = Config.get(section, 'Platform')

        login = LoginManager.LoginManager(email,password,security_hash,platform)
        login.login()
        func = WebAppFunctioner.WebAppFunctioner(login)
        print func.credits



if __name__ == "__main__":
    do_main()
```