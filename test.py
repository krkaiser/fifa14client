#!/usr/bin/env python

import sys
from Fifa14Client import LoginManager
from Fifa14Client import WebAppFunctioner
from Fifa14Client import fxn
import ConfigParser
from extra import EAHash
import time
import random
import datetime
import math
	
def do_main():
  #log_file = open('logfile', 'a')
  #sys.stdout = log_file
  global benatia_px
  global valencia_px
  global nani_px
  
  Config = ConfigParser.ConfigParser()
  Config.read("account4.ini")
  for section in Config.sections():
    email = Config.get(section, 'Email')
    password = Config.get(section, 'Password')
    secret_answer = Config.get(section, 'Secret')
    security_hash = EAHash.EAHashingAlgorithm().EAHash(secret_answer)
    platform = Config.get(section, 'Platform')
    login = LoginManager.LoginManager(email,password,security_hash,platform)
    
    st = datetime.datetime.now().strftime('%I:%M%p')
    #print 'BIN Check: ' + st
    if login.login() : 
      func = WebAppFunctioner.WebAppFunctioner(login)
      print func.get_leaderboard_stats('transfer')
      print func.get_leaderboard_stats('earnings')
      print func.get_leaderboard_stats('club_value')
      print func.get_leaderboard_stats('top_squad')
    #sys.stdout = sys.__stdout__
    #log_file.close()
    
if __name__ == "__main__":
    do_main()
