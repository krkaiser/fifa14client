#!/usr/bin/env python

import sys
from Fifa14Client import LoginManager
from Fifa14Client import WebAppFunctioner
from Fifa14Client import fxn
import ConfigParser
from extra import EAHash
import time
import random
import datetime
import math
	
def do_main():
  log_file = open('LV-logfile', 'a')
  sys.stdout = log_file
  global act3_id
  global act3_px
  global act4_id
  global act4_px
  global act5_id
  global act5_px
  global act6_id
  global act6_px
  global act7_id
  global act7_px
  global act8_id
  global act8_px
  
  coins = 0
  total_bal = 0
  act3_id = 49370 #Thiago Motta
  act3_px = 600
  act4_id = 115909 # Ruben Castro
  act4_px = 600
  act5_id = 196432 # Enzo Perez
  act5_px = 650
  act6_id = 169588 # Jonny Evans
  act6_px = 450
  act7_id = 173306 # Ansaldi
  act7_px = 500
  act8_id = 124375 # Yilmaz
  act8_px = 450
  
  Config = ConfigParser.ConfigParser()
  Config.read("/home/Fifa14Client/act/account3.ini")
  st = datetime.datetime.now().strftime('%I:%M%p')
  
  for section in Config.sections():
    email = Config.get(section, 'Email')
    password = Config.get(section, 'Password')
    secret_answer = Config.get(section, 'Secret')
    security_hash = EAHash.EAHashingAlgorithm().EAHash(secret_answer)
    platform = Config.get(section, 'Platform')
    login = LoginManager.LoginManager(email,password,security_hash,platform)

    print 'Starting: ' + st
    if login.login() : 
      func = WebAppFunctioner.WebAppFunctioner(login)
      coins = func.get_coin_amount()
      total_bal = total_bal + coins 
      print "Act 3 Coin balance: " + str(coins)
      print "Transfer Profit: " + str(func.get_leaderboard_stats('transfer'))
      fxn.clearWatch(func)
      relist(func)

      fxn.list_player(func,act3_id,act3_px)
      fxn.bid_player(func,act3_id,350)

      owned = fxn.ownedCards(func)
      for key in owned['count'] :
        print str(key) + ': ' + str(owned['count'][key]) + ' owned @ ' + str(owned['pxPaid'][key]/owned['count'][key])

  Config.read("/home/Fifa14Client/act/account4.ini")
  for section in Config.sections():
    email = Config.get(section, 'Email')
    password = Config.get(section, 'Password')
    secret_answer = Config.get(section, 'Secret')
    security_hash = EAHash.EAHashingAlgorithm().EAHash(secret_answer)
    platform = Config.get(section, 'Platform')
    login = LoginManager.LoginManager(email,password,security_hash,platform)

    if login.login() :
      func = WebAppFunctioner.WebAppFunctioner(login)
      coins = func.get_coin_amount()
      total_bal = total_bal + coins
      print "Act 4 Coin balance: " + str(coins)
      print "Transfer Profit: " + str(func.get_leaderboard_stats('transfer'))
      fxn.clearWatch(func)
      relist(func)

      fxn.list_player(func,act4_id,act4_px)
      fxn.bid_player(func,act4_id,400)
      
      owned = fxn.ownedCards(func)
      for key in owned['count'] :
        print str(key) + ': ' + str(owned['count'][key]) + ' owned @ ' + str(owned['pxPaid'][key]/owned['count'][key])

  Config.read("/home/Fifa14Client/act/account5.ini")
  for section in Config.sections():
    email = Config.get(section, 'Email')
    password = Config.get(section, 'Password')
    secret_answer = Config.get(section, 'Secret')
    security_hash = EAHash.EAHashingAlgorithm().EAHash(secret_answer)
    platform = Config.get(section, 'Platform')
    login = LoginManager.LoginManager(email,password,security_hash,platform)

    if login.login() :
      func = WebAppFunctioner.WebAppFunctioner(login)
      coins = func.get_coin_amount()
      total_bal = total_bal + coins
      print "Act 5 Coin balance: " + str(coins)
      print "Transfer Profit: " + str(func.get_leaderboard_stats('transfer'))
      fxn.clearWatch(func)
      relist(func)

      fxn.list_player(func,act5_id,act5_px)
      fxn.bid_player(func,act5_id,450)
   
      owned = fxn.ownedCards(func)
      for key in owned['count'] :
        print str(key) + ': ' + str(owned['count'][key]) + ' owned @ ' + str(owned['pxPaid'][key]/owned['count'][key])
    
    Config.read("/home/Fifa14Client/act/account6.ini")
  for section in Config.sections():
    email = Config.get(section, 'Email')
    password = Config.get(section, 'Password')
    secret_answer = Config.get(section, 'Secret')
    security_hash = EAHash.EAHashingAlgorithm().EAHash(secret_answer)
    platform = Config.get(section, 'Platform')
    login = LoginManager.LoginManager(email,password,security_hash,platform)

    if login.login() :
      func = WebAppFunctioner.WebAppFunctioner(login)
      coins = func.get_coin_amount()
      total_bal = total_bal + coins
      print "Act 6 Coin balance: " + str(coins)
      print "Transfer Profit: " + str(func.get_leaderboard_stats('transfer'))
      fxn.clearWatch(func)
      relist(func)

      fxn.list_player(func,act6_id,act6_px)
      fxn.bid_player(func,act6_id,300)
   
      owned = fxn.ownedCards(func)
      for key in owned['count'] :
        print str(key) + ': ' + str(owned['count'][key]) + ' owned @ ' + str(owned['pxPaid'][key]/owned['count'][key])
        
    Config.read("/home/Fifa14Client/act/account7.ini")
  for section in Config.sections():
    email = Config.get(section, 'Email')
    password = Config.get(section, 'Password')
    secret_answer = Config.get(section, 'Secret')
    security_hash = EAHash.EAHashingAlgorithm().EAHash(secret_answer)
    platform = Config.get(section, 'Platform')
    login = LoginManager.LoginManager(email,password,security_hash,platform)

    if login.login() :
      func = WebAppFunctioner.WebAppFunctioner(login)
      coins = func.get_coin_amount()
      total_bal = total_bal + coins
      print "Act 7 Coin balance: " + str(coins)
      print "Transfer Profit: " + str(func.get_leaderboard_stats('transfer'))
      fxn.clearWatch(func)
      relist(func)

      fxn.list_player(func,act7_id,act7_px)
      fxn.bid_player(func,act7_id,350)
   
      owned = fxn.ownedCards(func)
      for key in owned['count'] :
        print str(key) + ': ' + str(owned['count'][key]) + ' owned @ ' + str(owned['pxPaid'][key]/owned['count'][key])
        
    Config.read("/home/Fifa14Client/act/account8.ini")
  for section in Config.sections():
    email = Config.get(section, 'Email')
    password = Config.get(section, 'Password')
    secret_answer = Config.get(section, 'Secret')
    security_hash = EAHash.EAHashingAlgorithm().EAHash(secret_answer)
    platform = Config.get(section, 'Platform')
    login = LoginManager.LoginManager(email,password,security_hash,platform)

    if login.login() :
      func = WebAppFunctioner.WebAppFunctioner(login)
      coins = func.get_coin_amount()
      total_bal = total_bal + coins
      print "Act 8 Coin balance: " + str(coins)
      print "Transfer Profit: " + str(func.get_leaderboard_stats('transfer'))
      fxn.clearWatch(func)
      relist(func)

      fxn.list_player(func,act8_id,act8_px)
      fxn.bid_player(func,act8_id,300)
   
      owned = fxn.ownedCards(func)
      for key in owned['count'] :
        print str(key) + ': ' + str(owned['count'][key]) + ' owned @ ' + str(owned['pxPaid'][key]/owned['count'][key])

  print 'Run complete: ' + st + ' -> ' + datetime.datetime.now().strftime('%I:%M%p')
  print 'Total Balance: ' + str(total_bal)
  sys.stdout = sys.__stdout__
  log_file.close()

def relist(func, gap=5):
  tp = func.get_tradepile()
  soldCount = 0
  for card in tp:
    if card.expires == -1 and card.currentBid <> 0:
      print str(card.assetId) + ' sold for ' + str(card.currentBid)
      func.remove_from_tradepile(card)
      soldCount += 1
      continue
    elif card.expires == -1 or card.expires == 0:
      if card.assetId == act3_id:
        func.list_card(card,act3_px,act3_px+100,3600)
        print str(card.assetId) + ' relisted for ' + str(act3_px) + '/' + str(act3_px+100)
      elif card.assetId == act4_id :
        func.list_card(card,act4_px,act4_px+100,3600)
        print str(card.assetId) + ' relisted for ' + str(act4_px) + '/' + str(act4_px+100)
      elif card.assetId == act5_id :
        func.list_card(card,act5_px,act5_px+100,3600)
        print str(card.assetId) + ' relisted for ' + str(act5_px) + '/' + str(act5_px+100)
      elif card.assetId == act6_id :
        func.list_card(card,act6_px,act6_px+100,3600)
        print str(card.assetId) + ' relisted for ' + str(act6_px) + '/' + str(act6_px+100)
      elif card.assetId == act7_id :
        func.list_card(card,act7_px,act7_px+100,3600)
        print str(card.assetId) + ' relisted for ' + str(act7_px) + '/' + str(act7_px+100)
      elif card.assetId == act8_id :
        func.list_card(card,act8_px,act8_px+100,3600)
        print str(card.assetId) + ' relisted for ' + str(act8_px) + '/' + str(act8_px+100)
      elif card.expires == -1 :
        func.list_card(card,card.startingBid,card.buyNowPrice,3600)
        print str(card.assetId) + ' relisted for ' + str(card.startingBid) + '/' + str(card.buyNowPrice)
      time.sleep(gap)

if __name__ == "__main__":
    do_main()
