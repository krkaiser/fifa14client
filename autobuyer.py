#!/usr/bin/env python

import sys
from Fifa14Client import LoginManager
from Fifa14Client import WebAppFunctioner
from Fifa14Client import fxn
import ConfigParser
from extra import EAHash
import time
import random
import datetime
import math
	
def do_main():
  log_file = open('autobuyer-logfile', 'a')
  sys.stdout = log_file

  Config = ConfigParser.ConfigParser()
  Config.read("/home/Fifa14Client/act/account5.ini")
  for section in Config.sections():
    email = Config.get(section, 'Email')
    password = Config.get(section, 'Password')
    secret_answer = Config.get(section, 'Secret')
    security_hash = EAHash.EAHashingAlgorithm().EAHash(secret_answer)
    platform = Config.get(section, 'Platform')
    login = LoginManager.LoginManager(email,password,security_hash,platform)
    
    st = datetime.datetime.now().strftime('%I:%M%p')
    print 'Starting: ' + st
    if login.login() : 
      func = WebAppFunctioner.WebAppFunctioner(login)
      starting_bal = func.get_coin_amount()
      while True:
        #fxn.clearWatch(func) 
        relist(func)
        cards = func.search(type='player',num='50',definitionId='181872',maxb=700)
        if cards is None : continue
        for card in cards:
          if starting_bal < bid_amt: 
            print "Not enough coins to bid!"
            break
          else:
            func.bid(card,card.buyNowPrice)
            print 'Player bid!'
            starting_bal -= 700
        cards = func.search(type='player',num='50',definitionId='181872',macr=350)
        if cards is None : continue
        for card in cards:
          if starting_bal < 350: 
            print "Not enough coins to bid!"
            break
          elif int(card.expires) <= 60:
            func.bid(card,350)
            print 'Player bid!'
            starting_bal -= 350
    print 'Run complete: ' + st + ' -> ' + datetime.datetime.now().strftime('%I:%M%p')
    sys.stdout = sys.__stdout__
    log_file.close()

def relist(func):
  tp = func.get_tradepile()
  if tp is None : return  
  for card in tp:
    if card.expires == -1 and card.currentBid <> 0:
      print str(card.assetId) + ' sold for ' + str(card.currentBid)
      func.remove_from_tradepile(card)
      continue
    elif card.expires == -1:
      func.list_card(card,card.startingBid,card.buyNowPrice,3600)
      print str(card.assetId) + ' relisted for ' + str(card.startingBid) + '/' + str(card.buyNowPrice)

if __name__ == "__main__":
    do_main()
