#!/usr/bin/env python

import sys
from Fifa14Client import LoginManager
from Fifa14Client import WebAppFunctioner
from Fifa14Client import fxn
import ConfigParser
from extra import EAHash
import time
import random
import datetime
import math
	
def do_main():
  log_file = open('logfile', 'a')
  sys.stdout = log_file
  global benatia_px
  global valencia_px
  global nani_px
  
  Config = ConfigParser.ConfigParser()
  Config.read("/home/Fifa14Client/act/account2.ini")
  for section in Config.sections():
    email = Config.get(section, 'Email')
    password = Config.get(section, 'Password')
    secret_answer = Config.get(section, 'Secret')
    security_hash = EAHash.EAHashingAlgorithm().EAHash(secret_answer)
    platform = Config.get(section, 'Platform')
    login = LoginManager.LoginManager(email,password,security_hash,platform)
    
    st = datetime.datetime.now().strftime('%I:%M%p')
    print 'BIN Check: ' + st
    if login.login() : 
      func = WebAppFunctioner.WebAppFunctioner(login)
      #relist(func)
      fxn.minBin(func, 177509)
      fxn.minBin(func, 138956)
      fxn.minBin(func, 139068)
      fxn.minBin(func, 167905)
      fxn.minBin(func, 7763)
      fxn.minBin(func, 189795)
      fxn.minBin(func, 190483)
      fxn.minBin(func, 210513)
      fxn.minBin(func, 196432)
      fxn.minBin(func, 202166)
    sys.stdout = sys.__stdout__
    log_file.close()

def relist(func, gap=5):
  tp = func.get_tradepile()
  soldCount = 0
  for card in tp:
    if card.expires == -1 and card.currentBid <> 0:
      print str(card.assetId) + ' sold for ' + str(card.currentBid)
      func.remove_from_tradepile(card)
      soldCount += 1
      continue
  if card.expires == -1:
    func.list_card(card,card.startingBid,card.buyNowPrice,3600)
    print str(card.assetId) + ' relisted for ' + str(card.startingBid) + '/' + str(card.buyNowPrice)
  time.sleep(gap)

    
if __name__ == "__main__":
    do_main()
