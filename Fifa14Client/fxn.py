import time

def ifTest(func):
  cards = func.search(type='development',num='10',maskedDefId='5001003')
  count = 0
  print 'Number of cards returned: ' + str(len(cards))
  print cards
  
def ownedCards(func):
  wl = func.get_watchlist()
  tp = func.get_tradepile()
  
  d = {'pxPaid': {}, 'count': {}}

  for card in wl :
    if card.bidState == 'highest' and card.tradeState == 'closed' and card.itemState <> 'invalid' :
      if not card.assetId in d['count'] : 
        d['pxPaid'][card.assetId] = 0
        d['count'][card.assetId] = 0

      d['pxPaid'][card.assetId] += card.lastSalePrice
      d['count'][card.assetId] += 1

  for card in tp :
    if not card.assetId in d['count'] :
      d['pxPaid'][card.assetId] = 0
      d['count'][card.assetId] = 0

    d['pxPaid'][card.assetId] += card.lastSalePrice
    d['count'][card.assetId] += 1      
  
  return d

def printWatch(func):
  wl = func.get_watchlist()
  for card in wl:
    if card.bidState <> 'highest':
      print card

def printTP(func):
  tp = func.get_tradepile()
  for card in tp:
    print card

def incrementBid(startingBid) :       
  if startingBid < 10000 :
    return startingBid + 100
  elif startingBid < 50000 :
    return startingBid + 250
  elif startingBid < 100000 :
    return startingBid + 500
  elif startingBid > 100000 :
    return startingBid + 1000
    
def bidCalc(lowestBin) :
  if lowestBin < 1000 :
    return int(round(lowestBin*.95/50))*50 - 50
  elif lowestBin < 10000 :
    return int(round(lowestBin*.95/100))*100 - 100
  elif lowestBin < 50000 :
    return int(round(lowestBin*.95/250))*250 - 250
  elif lowestBin < 100000 :
    return int(round(lowestBin*.95/500))*500 - 500
  elif startingBid > 100000 :
    return 0

def clearWatch(func) :
  wl = func.get_watchlist()
  profit = 0
  sold = 0
 
  if not wl : return 

  for card in wl:
    '''if card.bidState == 'highest' and card.tradeState == 'closed' and card.currentBid == 300 and card.rareflag == 0 : 
        func.quicksell(card)
        sold += 1
        profit += (card.discardValue - 300)'''
    if card.bidState == 'outbid' :
      func.remove_card_from_watchlist(card)
  if sold > 0 :
    print "Number of cards quicksold: " + str(sold) + " - profit: " + str(profit)  
    
def list_player(func, defId, startingBid):
  wl = func.get_watchlist()
  tp = func.get_tradepile()
  cardCount = 0
  
  if startingBid < 10000 :
    buyNow = startingBid + 100
  elif startingBid < 50000 :
    buyNow = startingBid + 250
  elif startingBid < 100000 :
    buyNow = startingBid + 500
  elif startingBid > 100000 :
    #buyNow = startingBid + 1000
    raise
    
  tpSize = len(tp)
    
  for card in wl:
    
    if card.bidState == 'outbid' and card.itemState <> 'invalid': # and card.assetId == defId  
      func.remove_card_from_watchlist(card)
    if card.assetId == defId and card.bidState == 'highest' and card.tradeState == 'closed' and tpSize <= 48  and card.itemState <> 'invalid' :
    #if card.assetId == defId and tpSize <= 49  and card.itemState <> 'invalid' :
      func.move(card,'trade')
      tpSize += 1
      func.list_card(card,startingBid,buyNow,3600)
      print str(card.assetId) + ' listed for ' + str(startingBid) + '/' + str(buyNow)
      time.sleep(5)

  
      
def bid_player(func, defId, bid_amt):
  wl = func.get_watchlist()
  wlSize = len(wl) 
  
  #print wlSize

  if wlSize >= 99 : return
    
  starting_bal = func.get_coin_amount()
  
  count = 0
  #start = 0
  while count < 10 and count+len(wl) < 100:
    cards = func.search(type='player',num='50',definitionId=str(defId),macr=bid_amt)
    
    #print 'Number of cards returned: ' + str(len(cards))
    if cards is None : continue
    for card in cards:
      if starting_bal < bid_amt: 
        print "Not enough coins to bid!"
        break
      if int(card.expires) <= 3600 and card.contract >= 7 and card.currentBid < bid_amt and count+wlSize < 100:
        #print "Bidding card - WL " + str(wlSize+count)
        func.bid(card,bid_amt)
        starting_bal -= bid_amt
        count += 1
    if len(cards) > 0 : 
      break
  print 'Number of ' + str(defId) + ' bid: ' + str(count)
    
def bidDiscards(func):
  wl = func.get_watchlist()
  starting_bal = func.get_coin_amount()
  
  wlSize = len(wl)
  bid_amt = 300
  st = 100
  sold = 0
  bids = 0
  profit = 0
  coins = starting_bal
  
  for card in wl :
    #first sell all of the completed cards:
    if card.bidState == 'highest' and card.tradeState == 'closed' and card.currentBid == bid_amt and card.rareflag == 0 : 
        func.quicksell(card)
        sold += 1
        profit += (card.discardValue - bid_amt)
    elif card.bidState == 'outbid' and card.currentBid == bid_amt and card.rareflag == 0 :
      func.remove_card_from_watchlist(card)
        
  if sold > 0 :
    print "Number of cards quicksold: " + str(sold) + " - profit: " + str(profit) 
  
  while wlSize < 95 and coins > bid_amt :
    cards = func.search(type='player',num='50',lev='gold',macr=bid_amt,start=st)
    
    if cards is None or len(cards) == 0 : continue
    if len(cards) == 50 : st += 50
    
    for card in cards:
      if wlSize  > 95 :
        clearWatch(func)
        print 'Approaching WL limit!'
        break
      if starting_bal < bid_amt:
        coins = func.get_coin_amount()
        if coins < bid_amt :
          break
        else :
          starting_bal = coins
      if int(card.expires) <= 3600 and card.currentBid < bid_amt and card.rating > 75:
        try : 
          func.bid(card,bid_amt)
          starting_bal -= bid_amt
          bids += 1
          wlSize += 1
        except :
          pass
    if bids >= 50 : 
      break
    elif card.expires > 3600 :
      print "Search exhausted"
      break
    elif starting_bal < bid_amt :
      coins = func.get_coin_amount()
      if coins < bid_amt :
        print "Not enough coins to bid! Coins: " + str(coins)
        break
      else :
        starting_bal = coins
    time.sleep(1) 
  print "Number of discards bid: " + str(bids)

def minBin(func, defId, rat=0) :
  min = 10000000
  st = 0
  num = 0
  maxCt = 0
  
  while True :
    cards = func.search(type='player',num='50',definitionId=str(defId),start=st)
    #print 'Number of cards returned: ' + str(len(cards))
    #if len(cards) == 50 : 
    if cards is None : continue
    st += len(cards)
    if len(cards) == 0 : break
    for card in cards:
      if rat == 0 : rat = card.rating
      if card.buyNowPrice <= min and card.buyNowPrice <> 0 and card.rating == rat  and card.contract >= 5 :
        if card.buyNowPrice == min : 
          num += 1
          if maxCt < card.contract : maxCt = card.contract
        else : 
          num = 1
          min = card.buyNowPrice
          maxCt = card.contract
    
  if min <> 10000000 :
    print 'Lowest BIN (' + str(num) + ') for ' + str(defId) + ': ' + str(min) + ' / Contract:' + str(maxCt)
    print 'Number of cards: ' + str(st)
    return min
  else :
    print 'No BIN found!'
    return 0
