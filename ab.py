#!/usr/bin/env python

import sys
from Fifa14Client import LoginManager
from Fifa14Client import WebAppFunctioner
from Fifa14Client import fxn
import ConfigParser
from extra import EAHash
import time
import random
import datetime
import math
	
def do_main():
  log_file = open('logfile', 'a')
  sys.stdout = log_file
  global benatia_px
  global valencia_px
  global nani_px
  global chi_px
  global pirlo_px
  global gerrard_px

  benatia_px = 1200 
  valencia_px = 1200
  nani_px = 6000
  chi_px = 6200
  pirlo_px = 8600
  min = 0
  
  Config = ConfigParser.ConfigParser()
  Config.read("/home/Fifa14Client/act/account1.ini")
  for section in Config.sections():
    email = Config.get(section, 'Email')
    password = Config.get(section, 'Password')
    secret_answer = Config.get(section, 'Secret')
    security_hash = EAHash.EAHashingAlgorithm().EAHash(secret_answer)
    platform = Config.get(section, 'Platform')
    login = LoginManager.LoginManager(email,password,security_hash,platform)
    
    st = datetime.datetime.now().strftime('%I:%M%p')
    print 'Starting: ' + st
    if login.login() : 
      func = WebAppFunctioner.WebAppFunctioner(login)
      print "Coin balance: " + str(func.get_coin_amount())
      print "Transfer Profit: " + str(func.get_leaderboard_stats('transfer'))

      fxn.clearWatch(func) 
      relist(func)
   
      fxn.list_player(func,177509,benatia_px) # 177509 = Medhi Benatia
      fxn.bid_player(func,177509,700)
     
      fxn.list_player(func,7763,pirlo_px) # 7763 - Pirlo
      fxn.bid_player(func,7763,6200)

      fxn.list_player(func,138956,chi_px) #138956 - Giorgio Chiellini
      fxn.bid_player(func,138956,3500)
      
      fxn.list_player(func,139068,nani_px) # 139086 = Nani
      fxn.bid_player(func,139068,3500)
      
      fxn.list_player(func,167905,valencia_px) # 167905 = Antonia Valencia
      fxn.bid_player(func,167905,700)
      
      print "TP Size: " + str(len(func.get_tradepile()))
      print "WL Size: " + str(len(func.get_watchlist()))
      
      owned = fxn.ownedCards(func)
      for key in owned['count'] :
        print str(key) + ': ' + str(owned['count'][key]) + ' owned @ ' + str(owned['pxPaid'][key]/owned['count'][key])

    print 'Run complete: ' + st + ' -> ' + datetime.datetime.now().strftime('%I:%M%p')
   	
    sys.stdout = sys.__stdout__
    log_file.close()

def relist(func, gap=5):
  tp = func.get_tradepile()
  for card in tp:
    if card.expires == -1 and card.currentBid <> 0:
      print str(card.assetId) + ' sold for ' + str(card.currentBid)
      func.remove_from_tradepile(card)
      continue
    elif card.expires == -1 or card.expires == 0:
      if card.assetId == 177509 and card.rating == 82:
        func.list_card(card,benatia_px,benatia_px+100,3600)
        print str(card.assetId) + ' relisted for ' + str(benatia_px) + '/' + str(benatia_px+100)
      elif card.assetId == 167905 :
        func.list_card(card,valencia_px,valencia_px+100,3600)
        print str(card.assetId) + ' relisted for ' + str(valencia_px) + '/' + str(valencia_px+100)
      elif card.assetId == 139068 :
        func.list_card(card,nani_px,fxn.incrementBid(nani_px),3600)
        print str(card.assetId) + ' relisted for ' + str(nani_px) + '/' + str(fxn.incrementBid(nani_px))
      elif card.assetId == 138956 :
         func.list_card(card,chi_px,fxn.incrementBid(chi_px),3600)
         print str(card.assetId) + ' relisted for ' + str(chi_px) + '/' + str(fxn.incrementBid(chi_px))
      elif card.assetId == 7763 :
         func.list_card(card,pirlo_px,fxn.incrementBid(pirlo_px),3600)
         print str(card.assetId) + ' relisted for ' + str(pirlo_px) + '/' + str(fxn.incrementBid(pirlo_px))
      elif card.assetId == 45074 :
         func.list_card(card,40000,40250,3600)
         print str(card.assetId) + ' relisted for ' + str(40000) + '/' + str(fxn.incrementBid(40000))
      elif card.assetId == 0 :
        func.move(card,'club')
      elif card.expires == -1 :
        func.list_card(card,card.startingBid,card.buyNowPrice,3600)
        print str(card.assetId) + ' relisted for ' + str(card.startingBid) + '/' + str(card.buyNowPrice)
      time.sleep(gap)

if __name__ == "__main__":
    do_main()
