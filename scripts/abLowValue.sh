#!/bin/bash

lockfile=/tmp/lv.lock

if [ ! -e $lockfile ]; then
    touch $lockfile
    python /home/Fifa14Client/abLowValue.py
    rm $lockfile
else
    echo "critical-section is already running"
fi