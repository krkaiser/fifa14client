#!/bin/bash

lockfile=/tmp/ab.lock

if [ ! -e $lockfile ]; then
    touch $lockfile
    python /home/Fifa14Client/ab.py
    rm $lockfile
else
    echo "critical-section is already running"
fi