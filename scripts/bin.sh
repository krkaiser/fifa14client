#!/bin/bash

lockfile=/tmp/bin.lock

if [ ! -e $lockfile ]; then
    touch $lockfile
    python /home/Fifa14Client/bincheck.py
    rm $lockfile
else
    echo "critical-section is already running"
fi
